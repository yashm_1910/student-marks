# Student Marks

This app is made using Spring Boot to display grade of students after entering marks. 

## Getting Started


Please see the appropriate guide for your enviroment of choice 


* [JDK Download](https://www.oracle.com/java/technologies/javase-downloads.html) 
* [Spring Boot](https://spring.io)
* [Nodejs](https://nodejs.org) 
* [Maven](https://maven.apache.org/) 



## Built With


* [Spring Boot](https://spring.io) 
* [Maven](https://maven.apache.org/) 




## Authors

* **Yash Mahajan** - *Initial work* - [yashm_1910](https://bitbucket.org/yashm_1910)
