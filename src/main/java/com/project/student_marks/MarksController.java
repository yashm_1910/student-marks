package com.project.student_marks;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MarksController {  
    
    SubjectMarks marks = new SubjectMarks();

    @GetMapping("/Grade")
    public ModelAndView index(Model model){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("grade.html");
        return mav;
    }


    @PostMapping(value="/Grade")
    public ModelAndView displayGrade(
        @RequestParam String Physics, 
        @RequestParam String Chemistry, 
        @RequestParam String Maths, 
        Model model ){
            Double phy;
            Double chem;
            Double math;

            try {
                phy = Double.parseDouble(Physics);
            } catch (NumberFormatException ex) {
                phy =0D;
            }
            try {
                chem = Double.parseDouble(Chemistry);
            } catch (NumberFormatException ex) {
                chem =0D;
            }
            try {
                math = Double.parseDouble(Maths);
            } catch (NumberFormatException ex) {
                math =0D;
            }

            marks.setPhy(phy);
            marks.setChem(chem);
            marks.setMath(math);
            Double total = marks.totalMarks();
            Double avg = marks.averageMarks();
            String grade = marks.grade();

            ModelAndView mav = new ModelAndView();

            mav.addObject("Physics", phy);
            mav.addObject("Chemistry", chem);
            mav.addObject("Maths", math);
            mav.addObject("total", total);
            mav.addObject("avg", avg);
            mav.addObject("grade", grade);
            
            mav.setViewName("grade.html");

        return mav;
    }


}