package com.project.student_marks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentMarksApp {

	public static void main(String[] args) {
		SpringApplication.run(StudentMarksApp.class, args);
	}

}
