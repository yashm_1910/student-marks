package com.project.student_marks;



public class SubjectMarks {
    Double phy;
    Double chem;
    Double math;

    public SubjectMarks() {
    }

    public Double getPhy() {
        return phy;
    }

    public void setPhy(Double phy) {
        this.phy = phy;
    }

    public Double getChem() {
        return chem;
    }

    public void setChem(Double chem) {
        this.chem = chem;
    }

    public Double getMath() {
        return math;
    }

    public void setMath(Double math) {
        this.math = math;
    }


    public Double totalMarks(){
        Double total = phy + chem + math;
        return total;

    }

    public Double averageMarks (){
        Double average = totalMarks( ) / 3;
        return average;
    }

    public String grade(){
        String grade;
        if (averageMarks()>=80) {
            grade = "A";
        } 
        else if (averageMarks()>=50) {
            grade = "B";
        } else {
            grade = "C";
        }

      return grade;
    }
    

}